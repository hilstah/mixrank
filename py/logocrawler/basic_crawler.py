import requests
import pandas as pd
import re
 
HEADERS = {
    'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
    'Accept-Encoding' : 'gzip, deflate, br, zstd',
    'Accept-Language' : 'en-US,en;q=0.9,pt;q=0.8',
    'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'
}

URL_REGEX = r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})'

def filter_urls(url):

    filters = ['icon', 'png', 'logo', 'svg']

    for filter in filters:

        if filter in url:

            return True

def try_get_content(url):


    print(url)

    try:

        res = requests.get('https://'+url, timeout=10, headers=HEADERS)


    except Exception as e:

        return {'domain' : url, 'content' : str(e)}

        

    html = str(res.content)

    return {'domain' : url, 'content' : html}

def parse_content_with_regex(content, url):

    urls_from_html = re.findall(
    URL_REGEX,
    content
    )

    if len(urls_from_html) > 0:

        candidates = [ufh for ufh in urls_from_html if filter_urls(ufh)]

        return {'domain' : url, 'logo' : candidates[:5]}

    else:

        return {'domain' : url, 'logo' : None}

domains_list = pd.read_csv('../../websites.csv', header=None)[0].to_list()

results_records = []

for url in domains_list:

    content_obj = try_get_content(url=url)
    results_records.append(
        parse_content_with_regex(content=content_obj['content'], url=url)
        )
    
pd.DataFrame(results_records).to_csv('../../basic_crawler_results.csv')