import aiohttp 
import asyncio
import pandas as pd 
from bs4 import BeautifulSoup


HEADERS = {
    'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
    'Accept-Encoding' : 'gzip, deflate, br, zstd',
    'Accept-Language' : 'en-US,en;q=0.9,pt;q=0.8',
    'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'
}

def parse_with_bs4(content):
    s = BeautifulSoup(content, 'html.parser')
    classes = []
    for i in s.find_all(class_=True):
        classes.extend(i['class'])

    filtered_classes = [s.find_all(class_=i)[0] for i in classes if ('logo' in i) or ('icon' in i)]

    srcs = []

    if len(filtered_classes) > 0:

        for fc in filtered_classes:
            try:
                if fc.get("src"):
                    srcs.append(fc.get("src"))

            except:
                continue 

            images = fc.find_all("img")
            
            
            if len(images) > 0:
                for im in images:
                    if im.get("src"):
                        srcs.append(im.get("src"))

        return srcs[:5]
    
    else:

        return None

async def get(url, session):
    try:
        async with session.get(url=url) as response:
            resp = await response.read()
            
            return {'domain' : url, 'content' : resp}
    except Exception as e:
        
        
        return {'domain' : url, 'content' : str(e)}

async def main(urls):
    async with aiohttp.ClientSession(
        headers=HEADERS,
        connector=aiohttp.TCPConnector(ssl=False),
        timeout= aiohttp.ClientTimeout(total=60)
    ) as session:
        r = await asyncio.gather(*(get('https://'+url, session) for url in urls))

        return r


domains_list = pd.read_csv('../../websites.csv', header=None)[0].to_list()

contents = asyncio.run(main(domains_list))

result_records = []

for c in contents:

    logos = parse_with_bs4(c['content'])

    result_records.append(
        {'domain' : c['domain'], 'logo' : logos}
    )


pd.DataFrame(result_records).to_csv('../../3rd_crawler_results.csv')